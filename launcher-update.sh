#!/bin/bash
set -euo pipefail

# either set DEBUG= something non-empty after the declaration here or run the script via
# `DEBUG=y bash launcher-update.sh` or `debug=y bash launcher-update.sh`
#
# don't comment this declaration out or `set -u` will exit the script the first time DEBUG is checked
: ${DEBUG:=${debug:=""}}

# some debug print macros
ddeclare() { [[ -z $DEBUG ]] || declare -p "$@"; }
dprint() { [[ -z $DEBUG ]] || echo "$@"; }

startdir=$(pwd)
ddeclare startdir

# TODO:
#       implement a usage/help function
#       implement operating on a target directory; eg: run from ~ install to ~/.local/share/eve-online or something
#       implement some kind of version guessing since the launcher self-updates; our .lastrelease isn't always accurate
#       maybe be fancy and use fzf to select where to install
#       maybe be fancy and use fzf when guessing current version
#
# FIXME:
#       
#       update any files in ./packages/ so the launcher knows what versions are available after our update
#       implement tmpdir cleanup as an exit hook; handle ctrl-c case as well
#       cleanup any prior versions in ./app-$version/ after update

# semver comparison courtesy of https://stackoverflow.com/a/4024263/12508057
#
# version less than or equal to
verlte() {
  [[  "$1" = "$(echo -e "$1\n$2" | sort -V | head -n1)" ]]
}

# version less than
verlt() {
  [[ "$1" = "$2" ]] && return 1 || verlte $1 $2
}

# Current download URL formats look something like:
#  https://launcher.ccpgames.com/eve-online/release/win32/x64/RELEASES
#  https://launcher.ccpgames.com/eve-online/release/win32/x64/eve-online-0.2.9-full.nupkg
#
urlbase="https://launcher.ccpgames.com/eve-online/release/win32/x64"

# get releases
declare -a RELEASES
readarray -t RELEASES <<< "$(curl -s "${urlbase}/RELEASES")"
ddeclare RELEASES

# sift through releases discarding deltas and find the highest full version
declare -a RELEASE
declare -a tRELEASE
currver=''
for (( i=1; i<=${#RELEASES[@]}; i++ )); do
  # skip this element if it's not a full release package
  [[ "${RELEASES[-$i]}" =~ .*-full.nupkg.* ]] || continue

  # read release data into its own array
  oIFS=$IFS
  IFS=' ' read -ra tRELEASE <<< "${RELEASES[-$i]}"
  IFS=$oIFS
  ddeclare tRELEASE

  # strip release filename down to its semver
  iver="${tRELEASE[1]%-full.nupkg}"   # strip trailing
  iver="${iver#eve-online-}"          # strip leading
  ddeclare iver

  # compare this release to the highest version we've seen so far and store version if higher
  if [[ -z "$currver" ]] || verlte "$currver" "$iver"; then
    currver="$iver"
    RELEASE=("${tRELEASE[@]}")
    ddeclare currver
  fi
done

ddeclare currver
ddeclare RELEASE

# if RELEASE data is empty we've failed
if ! (( ${#RELEASE[@]} )); then
  echo "Couldn't fetch current release, something broke :("
  exit 1
fi

# current release data fetched, get current package filename
currpkg="${RELEASE[-2]}"
ddeclare currpkg

# and current package url
pkg_url="${urlbase}/${currpkg}"
ddeclare pkg_url

# FIXME: we should probably try to determine which version is currently installed
#        since the launcher can update itself our .lastrelease may or may not be accurate
# look up our stored last version
[[ -f .lastrelease ]] || touch .lastrelease
read -r last < .lastrelease || last=""
ddeclare last

# if our last release is lower than current we need to update
if verlt "$last" "$currver"; then

    # FIXME: implement tmpdir cleanup via an exit hook so we clean up after ourselves
    #        even if we're CTRL-C'd

    # make a temp working directory
    tmpdir=$(mktemp -d -p "$(pwd)" ".tmp.XXXXXXXX")
    ddeclare tmpdir
    cd "$tmpdir"

    # get the package and unpack it
    if [[ -f "$startdir/.cached/$currpkg" ]]; then
      # check our cache first
      cp --reflink=auto "$startdir/.cached/$currpkg" .
    else
      # otherwise fetch it
      wget "$pkg_url"
    fi
    unzip "$currpkg"

    # move the launcher package into .cached/ in case we need it again
    if [[ -f "$startdir/.cached/$currpkg" ]]; then
      # just remove our copy if we've already cached it
      rm -f "$currpkg"
    else
      mkdir -p .cached
      mv -f "$currpkg" .cached/
    fi

    # copy launcher files into place
    cp --reflink=auto -af lib/net45 app-$currver
    cp --reflink=auto -f lib/net45/squirrel.exe Update.exe
    cp --reflink=auto -f lib/net45/eve-online_ExecutionStub.exe eve-online.exe

    # save this current version as our last release
    echo "$currver" > .lastrelease

    # move everything up into place in the launcher directory
    cp --reflink=auto -af -- * "$startdir/"
    cd "$startdir"

    # FIXME: actually clean up after ourselves at some point
    #        this should be done a little more safely than rm -rf "$var"
    #        even with the "exit script if variable is unset" guardrails I don't like using `rm -rf` from a script
    echo "rm -rf $tmpdir"
  else
    # we're up to date
    echo "launcher appears to be up to date :-)"
fi

# vim: set ts=4 sts=2 sw=2 et nowrap:
